#example line for "Neck" slot
#* Neck: Drake Fang Amulet

#list of all slots:

# Head Ears1 Ears2 Face Chest Arms Back Waist Shoulders Wrists1 Wrists2 Legs Hands
# Fingers1 Fingers2 Feet Primary Secondary Range Ammo

import Tkinter
import tkFileDialog
import csv
import os
import time
import sys

helpText = '''
=========== Inventory Parser and Consolidation Information ===========

The inventory files output by Everquest do not indicate
which character they are for.

Due to this, you must output the files in a format that
tell which character created it.

Use this command in game, but substitute your
character name (my character is Oggrukk):

/outputfile inventory Oggrukk_inventory.txt

================== Enjoy :) ==================
'''

strItemSlots = 'Ears1 Head Face Ears2 Neck Shoulders Arms Back Wrists1 Wrists2 Range Hands Primary Secondary Fingers1 Fingers2 Chest Legs Feet Waist Ammo'
arrItemSlots = strItemSlots.split(' ')
print arrItemSlots
print helpText
try:
    pauseForEnter = input("Press enter to continue...")
except SyntaxError:
    pass
if sys.argv.count == 2:
    folderPath = sys.argv[1]
    outFilePath = sys.argv[2]
else:
    root = Tkinter.Tk()
    root.withdraw()
    objFolderPath = tkFileDialog.askdirectory(title='Select your Everquest directory')
    objOutFilePath = tkFileDialog.asksaveasfile(title='Select your output file location', initialfile='magelo_output.txt')
    if objFolderPath and objOutFilePath:
        folderPath = str(objFolderPath)
        outFilePath = str(objOutFilePath.name)
    else:
        folderPath = None
        outFilePath = None

if not folderPath or not outFilePath:
    print("Everquest folder or output file location not selected. The script will now exit.")
    try:
        pauseForEnter = input("Press enter to continue...")
    except SyntaxError:
        pass
    exit()
invFiles = os.listdir(folderPath)
print 'The following files are available:\n'
intIndex = 1
fileArray = []
for fileName in invFiles:
    if fileName.endswith('_inventory.txt'):
        fileArray.append(fileName)
for fileName in fileArray:
        fullPath = folderPath + '\\' + fileName
        thisFileCreateTime = time.ctime(os.path.getmtime(fullPath))
        print str(intIndex) + ' ' + fileName + ' ' + thisFileCreateTime
        intIndex = intIndex + 1
print '\nChoose the number of the file above to process...'
promptChoice = input('File number: ')
try:
    strFileNameChoice = fileArray[(promptChoice - 1)]
except:
    print 'Please choose a valid entry.\n\nExiting...'
    exit()
with open(folderPath + '\\' + strFileNameChoice,'r') as f:
    strContent = f.readlines()
intIndex = 2 #set the index for the next loop
with open(outFilePath,'w') as outFile:
    for strItemSlot in arrItemSlots:
        outFile.write('* ' + strItemSlot + ': ' + strContent[intIndex].split('\t')[1].replace('Empty','') + '\n')
        intIndex = intIndex + 1