# in game, run this command:
#
# /outputfile inventory {CHARACTER_NAME}_inventory.txt
#

import Tkinter
import tkFileDialog
import csv
import os
import time
import sys

helpText = '''
=========== Inventory Parser and Consolidation Information ===========

The inventory files output by Everquest do not indicate
which character they are for.

Due to this, you must output the files in a format that
tell which character created it.

Use this command in game, but substitute your
character name (my character is Oggrukk):

/outputfile inventory Oggrukk_inventory.txt

Do this once with each character, THEN run this script.

================== Enjoy :) ==================
'''
print helpText
try:
    pauseForEnter = input("Press enter to continue...")
except SyntaxError:
    pass
if sys.argv.count == 2:
    folderPath = sys.argv[1]
    outFilePath = sys.argv[2]
else:
    root = Tkinter.Tk()
    root.withdraw()
    objFolderPath = tkFileDialog.askdirectory(title='Select your Everquest directory')
    objOutFilePath = tkFileDialog.asksaveasfile(title='Select your output file location', initialfile='inventory_consolidated.csv')
    if objFolderPath and objOutFilePath:
        folderPath = str(objFolderPath)
        outFilePath = str(objOutFilePath.name)
    else:
        folderPath = None
        outFilePath = None

if not folderPath or not outFilePath:
    print("Everquest folder or output file location not selected. The script will now exit.")
    try:
        pauseForEnter = input("Press enter to continue...")
    except SyntaxError:
        pass
    exit()

invFiles = os.listdir(folderPath)

with open(outFilePath, 'w') as outFile:
    outFile.write(str('"' + 'Character' + '","' + 'Location'+ '","' + 'Item' + '","'  + 'LastSeenTime' + '","' + 'CountInStack' + '"\n'))

with open (outFilePath, 'a') as outFile:
    for fileName in invFiles:
        if fileName.endswith('_inventory.txt'):
            print 'processing file ' + fileName
            fullPath = folderPath + '\\' + fileName
            print 'fullpath='+ fullPath
            lastModified = time.ctime(os.path.getmtime(fullPath))
            print 'lastmodified='+ lastModified
            charName = os.path.split(fullPath)[1].split('_')[0]
            print 'charname='+ charName
            with open((fullPath), 'r') as csvFile:
                csvReader = csv.reader(csvFile, delimiter='\t')
                for row in csvReader:
                    if row[0].startswith(('General','Held')) and row[1] != 'Empty' and row[0] != 'Location':
                        outFile.write('"' + charName + '","' + 'Inventory'+ '","' + row[1] + '","' + lastModified + '","' + row[3] + '"\n')
                    elif row[0].startswith(('Bank','SharedBank')) and row[1] != 'Empty' and row[0] != 'Location':
                        outFile.write('"' + charName + '","' + 'Bank' + '","' + row[1] + '","' + lastModified + '","' + row[3] + '"\n')
                    elif row[1] != 'Empty' and row[0] and row[0] != 'Location':
                        outFile.write('"' + charName + '","' + 'Equipped' + '","' + row[1] + '","' + lastModified + '","' + row[3] + '"\n')
try:
    pauseForEnter = input("Script complete. Press enter to continue...")
except SyntaxError:
    pass